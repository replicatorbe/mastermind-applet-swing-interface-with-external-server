package ctrl;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


import commun.ExceptionJeu;

import model.ConfigEnvironnement;
import model.Partie;
import vue.JeuGraphique;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */


public class MenuItemCtrl implements ActionListener, KeyListener, WindowListener {

	private JeuGraphique vue;
	private Partie partie;

	public MenuItemCtrl(JeuGraphique vue, Partie partie) {
		this.vue=vue;
		this.partie=partie;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {

			if (arg0.getActionCommand().equals("Valider")){

				int bouleRouge = verif_boule_rouge();
				int bouleBlanche = verif_boule_blanche();
				System.out.println("BB"+bouleRouge);
				vue.mettreBoule(0, bouleRouge);
				vue.mettreBoule(1, bouleBlanche);
			
				partie.getReponse()[partie.getLigne()][0].setBoule(bouleRouge);
				partie.getReponse()[partie.getLigne()][1].setBoule(bouleBlanche);

				remise_a_zero();
				changementLigne();			

				//supprime les boules sur l'�cran
				vue.detectionEcran();
				//affiche la solution
				partie.getSolution();



			} else if (arg0.getActionCommand().equals("Anneau")){ 
				setSkin("Anneau");
				vue.refresh();
			} 
			else if (arg0.getActionCommand().equals("Triangle")) { 
				setSkin("Triangle");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Cone")) { 
				setSkin("Cone");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Canette")) { 
				setSkin("Canette");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Bouteille")) { 
				setSkin("Bouteille");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Cube")) { 
				setSkin("Cube");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Boule")) { 
				setSkin("Default");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Chapeau")) { 
				setSkin("Chapeau");
				vue.refresh();
			} else if (arg0.getActionCommand().equals("Fermer la partie")) { 
				vue.dispose();
			} else if (arg0.getActionCommand().equals("Nouvelle partie")) {
				Partie modele = new Partie();
				//colonne/nbredecouleur/essai///
				//	modele.configuration(4,5,6);
				modele.configuration();
				JeuGraphique vue = new JeuGraphique();
				vue.setModele(modele);

			}

		} catch (ExceptionJeu e) {
			traiter_err(e);
		}
	}

	public void changementLigne() throws ExceptionJeu {  
		//partie perdue
		if (partie.getLigne() == partie.getNbreLigne()-1) {
			throw new ExceptionJeu(ExceptionJeu.typeErreur.PERDANT);
		}

		partie.changementLigne();
	}


	public void remise_a_zero() throws ExceptionJeu {
		partie.remise_a_zero();  
	}

	public int verif_boule_blanche() throws ExceptionJeu {

		int boule = 0;
		String con = "";

		try {
			for (int k=0; k<ConfigEnvironnement.getInstance().getNbreColonne(); k++) 
				con+=partie.getProposition()[partie.getLigne()][k].getCouleur()+"/";
			URL url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom=" + ConfigEnvironnement.getInstance().getPseudo() + "&bouleblanche="+con);
			System.out.println(url.toString());
			URLConnection urlConnection = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
			{
				boule=Integer.parseInt(line);
			}
			in.close(); 
		} 
		catch (IOException erreur) {
			//
		}

		return boule;


	}


	public int recupNewNumeroPartie() throws ExceptionJeu {

		int boule = 0;

		try {
			URL url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom1="+ConfigEnvironnement.getInstance().getPseudo() +"&colonne=" + ConfigEnvironnement.getInstance().getNbreColonne() + "&ligne=" + ConfigEnvironnement.getInstance().getNbreLigne() + "&couleur=" + ConfigEnvironnement.getInstance().getNbreCouleur());
			System.out.println(url.toString());
			URLConnection urlConnection = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
			{
				boule=Integer.parseInt(line);
			}
			
			//on r�cupere le nouveau numero
			ConfigEnvironnement.getInstance().setId(boule);
			in.close(); 
		} 
		
		
		catch (IOException erreur) {
			//
		}

		return boule;


	}

	public int verif_boule_rouge() throws ExceptionJeu {

		System.out.println(partie.getColonne());
		if (partie.getLigne()==0) {
		//	this.joueur();
			this.recupNewNumeroPartie();
		}
		
		int boule = 0;

		try {
		//	URL url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?numeropartie=" + ConfigEnvironnement.getInstance().getId() + "&boulerouge="+partie.getProposition()[partie.getLigne()][0].getCouleur()+"/"+partie.getProposition()[partie.getLigne()][1].getCouleur()+"/"+partie.getProposition()[partie.getLigne()][2].getCouleur()+"/"+partie.getProposition()[partie.getLigne()][3].getCouleur());
			String con = "";
			
			for (int k=0; k<ConfigEnvironnement.getInstance().getNbreColonne(); k++) 
			con+=partie.getProposition()[partie.getLigne()][k].getCouleur()+"/";
			URL url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom=" + ConfigEnvironnement.getInstance().getPseudo() + "&boulerouge="+con);
			
			System.out.println(url.toString());
			URLConnection urlConnection = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
			{
				boule=Integer.parseInt(line);
			}
			in.close(); 
		} 
		catch (IOException erreur) {
			//
		}

		if (boule == partie.getNbreColonne()) {
			
			throw new ExceptionJeu(ExceptionJeu.typeErreur.GAGNANT);
		}
		
		return boule;
	}

	public int joueur() throws ExceptionJeu {

		int boule = 0;

		try {
			URL url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom1=" + ConfigEnvironnement.getInstance().getPseudo());
			System.out.println(url.toString());
			URLConnection urlConnection = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
			{
				boule=Integer.parseInt(line);
			}
			
			//on r�cupere le nouveau numero
			ConfigEnvironnement.getInstance().setId(boule);
			in.close(); 
		} 
		
		
		catch (IOException erreur) {
			//
		}

		return boule;


	}
	
	public int getColonne() throws ExceptionJeu {
		return partie.getColonne();
	}

	private void setSkin (String skin) throws ExceptionJeu {

		if (getColonne()>0) {
			throw new ExceptionJeu(ExceptionJeu.typeErreur.SKIN);
		}

		ConfigEnvironnement.getInstance().setTheme(skin);
		//		JeuGraphique.setSkin(skin);
	}



	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_ENTER) {
			Toolkit.getDefaultToolkit().beep();
			try {

				int bouleRouge = verif_boule_rouge();
				int bouleBlanche = verif_boule_blanche();

				vue.mettreBoule(0, bouleRouge);
				vue.mettreBoule(1, bouleBlanche);

				partie.getReponse()[partie.getLigne()][0].setBoule(bouleRouge);
				partie.getReponse()[partie.getLigne()][1].setBoule(bouleBlanche);

				remise_a_zero();
				changementLigne();			

				//supprime les boules sur l'�cran
				vue.detectionEcran();
				//affiche la solution
				partie.getSolution();
			} catch (ExceptionJeu ex) {
				traiter_err(ex);
			}

		} else if (key == KeyEvent.VK_X) {
			vue.dispose();
		} else if (key == KeyEvent.VK_R) {
			partie.videReponse();
			partie.resetEtatSol();
			partie.resetTabReponse();
			partie.zero();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	private void traiter_err(ExceptionJeu e) {
		switch (e.getErreur()) {
		case TIMERS:
			//choix level incorrect
			vue.alerteTimers();
			break;
		case NBR_ELEVE:
			//choix couleur incorrect
			vue.nombreEleve();
			break;
			//gagnant
		case GAGNANT:
			findepartie();
			if (vue.win() == 1) {
				Partie modele = new Partie();
				modele.configuration();
				JeuGraphique vue = new JeuGraphique();
				vue.setModele(modele);
			} else {
				URL url;
				try {
					url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom="+ConfigEnvironnement.getInstance().getPseudo() +"&findepartie=ok");
					URLConnection urlConnection = url.openConnection();
					BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					in.close(); 
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception ee) {
					// TODO Auto-generated catch block
					
				}
			}
			break; 
			//perdant
		case PERDANT:
			findepartie();
			if (vue.lose() == 1) {
				Partie modele = new Partie();
				modele.configuration();
				JeuGraphique vue = new JeuGraphique();
				vue.setModele(modele);
			} else {
				URL url;
				try {
					url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom="+ConfigEnvironnement.getInstance().getPseudo() +"&findepartie=ok");
					URLConnection urlConnection = url.openConnection();
					BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					in.close(); 
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception ee) {
					// TODO Auto-generated catch block
					
				}
			}
			break; 
		case SKIN:
			vue.alerteSkin();
			break; 
		case AUTRE:
			break;
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
		findepartie();
			
	}

	public void findepartie() {
		URL url;
		try {
			url = new URL("http://localhost:8080/SpringMVC_Annotation/jeu.htm?nom="+ConfigEnvironnement.getInstance().getPseudo() +"&findepartie=ok");
			URLConnection urlConnection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			in.close(); 
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception ee) {
			// TODO Auto-generated catch block
			
		}
	
	}
	
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
