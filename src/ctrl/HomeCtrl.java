
package ctrl;

import java.applet.Applet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import model.Partie;
import vue.FenetreConfiguration;
import vue.FenetreCredits;
import vue.FenetreHome;
import vue.JeuGraphique;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */

public class HomeCtrl extends Applet implements ActionListener, MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HomeCtrl() {
	}
	
	public void init (){  
		
	//	ConfigEnvironnement.getInstance().setId(Integer.parseInt(getParameter("id")));
		FenetreHome f = new FenetreHome();
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {

		String source = e.getActionCommand();
		
		if(source == "Jouer"){

			Partie modele = new Partie();
			//colonne/nbredecouleur/essai///
			//	modele.configuration(4,5,6);
			modele.configuration();
		
			JeuGraphique vue = new JeuGraphique();
			vue.setModele(modele);


		}  	else if (source == "Credit"){
			@SuppressWarnings("unused")
			FenetreCredits f = new FenetreCredits();
		}	else if (source == "Configuration"){
			@SuppressWarnings("unused")
			FenetreConfiguration fc = new FenetreConfiguration();
		}	

	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
