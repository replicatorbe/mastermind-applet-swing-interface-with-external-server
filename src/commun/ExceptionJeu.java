

package commun;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */

public class ExceptionJeu extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ExceptionJeu(typeErreur err) {
		this.err = err;
	}

	public typeErreur getErreur() {
		return err;
	}

	public enum typeErreur {JOUEUR_EXISTE, JOUEUR_EXISTE_PAS, GAGNANT, PERDANT, NBR_ELEVE, SKIN, AUTRE, TIMERS};
	private typeErreur err;
}
