/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ConfigEnvironnement;

import ctrl.*;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */
public class FenetreHome extends Applet {

	private static final long serialVersionUID = 646735462947799696L;
	private JPanel panel = new JPanel(new BorderLayout());
	private JPanel panel2 = new JPanel();
	private PanneauHome panel3 = new PanneauHome();
	private Thread t1 = new Thread(panel3);
	private JPanel panel4 = new JPanel(new GridLayout(0,4));
//	private Image icone = Toolkit.getDefaultToolkit().getImage("petit.png");


	public void init (){  
		
		//	System.setSecurityManager(null);
			//@SuppressWarnings("unused")
		
	//	ConfigEnvironnement.getInstance().setId(Integer.parseInt(getParameter("id")));
		
		this.setSize(600, 600);
		this.add(panel);
	//	this.setPreferredSize(getSize());
		//Toolkit tkit = getToolkit();
 		//this.setLocation((tkit.getScreenSize().width - this.getSize().width) / 2, (tkit.getScreenSize().height - this.getSize().height) / 2);
		//this.add(panel);
		//panel.setSize(800, 800);
		
	
		}
	
	public FenetreHome() {
		//titre
	//	this.setTitle("MasterMind");
		//this.setSize(800,800);
	//	this.setResizable(false);
		//this.setContentPane(panel);
		
	//	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		//this.setIconImage(icone);;
		panel.setOpaque(false);
		panel4.setOpaque(false);
		panel.add(panel3, BorderLayout.CENTER);
		panel3.add(panel4);
		panel4.add(panel2);
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.PAGE_AXIS));
		//on rend transparent le panel
		panel2.setOpaque(false);
		//panel2.add(Box.createHorizontalStrut(-500));
		panel2.add(Box.createHorizontalStrut(120));
		panel2.add(Box.createVerticalStrut(130));
		
		addAButton("Jouer", panel2);
		panel2.add(Box.createVerticalStrut(30));
		addAButton("Configuration", panel2);
		panel2.add(Box.createVerticalStrut(30));
		addAButton("Credit", panel2);
		panel2.add(Box.createVerticalStrut(175));
		
		
		t1.start();
		
		JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane();
		String nom = jop.showInputDialog(null, "Votre pseudo svp !", "Votre pseudo !", JOptionPane.QUESTION_MESSAGE);
		jop2.showMessageDialog(null, "Bienvenue " + nom, "Identit�", JOptionPane.INFORMATION_MESSAGE);
		ConfigEnvironnement.getInstance().setPseudo(nom);

		
	
		
		//this.setVisible(true);
	}


	private static void setPseudo(String nom) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * ajouter un bouton dans le menu
	 * @param text
	 * @param container
	 */
	private static void addAButton(String text, Container container) {    
		JButton button = new JButton(text);        
		button.setPreferredSize(new Dimension(110,40));
		button.setMaximumSize(button.getPreferredSize());
		button.addActionListener(new HomeCtrl());
		button.addMouseListener(new HomeCtrl());
		button.setOpaque(true);
		button.setBorderPainted(true);
		button.setForeground(Color.red);
		button.setContentAreaFilled(false);
		button.setRolloverEnabled(true);
		button.setCursor(Cursor.getPredefinedCursor(12));
		container.add(button);  
	}
}   

