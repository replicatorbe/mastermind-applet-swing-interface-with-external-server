
package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */
public class PanneauFond extends JPanel {

	private static final long serialVersionUID = 5389737475639435836L;
	private String filePath="configuration.png";


	public void paintComponent(Graphics g) {

		try {
			BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/"+filePath));          
			g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);                                       

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}