/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */
public class PanneauHome extends JPanel implements Runnable {

	private static final long serialVersionUID = 5389737475639435836L;
	private String filePath="fond.png";

	@Override
	public void run() {
		// TODO Auto-generated method stub

		try {
			while (true) {
				Thread.sleep(2000);
				filePath="fond.png";
				this.repaint();
				Thread.sleep(2000);
				filePath="fondb.png";
				this.repaint();
				Thread.sleep(1000);
				filePath="fond.png";
				this.repaint();
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void paintComponent(Graphics g) {

		
		
		BufferedImage image;
		try {
	//		System.out.println(System.getProperty("user.dir"));
			image = ImageIO.read(getClass().getResourceAsStream("/"+filePath));
			g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);                                       
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}             
	

	}

}