
package model;

import javax.swing.JLabel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 2.0
 * 
 */

public class Boule extends JLabel {

	
	private static final long serialVersionUID = 1L;
	private int couleur = 0;
	private boolean etat = false;
	private boolean drag = false;
	private int position = 0;

	public Boule() {
	}

	public Boule(int couleur, boolean etat) {
		this.couleur = couleur;
		this.etat = etat;
	}

	public void setCouleur (int couleur) {
		this.couleur=couleur;
	}

	public void setEtat (boolean etat) {
		this.etat=etat;
	}

	public int getCouleur() {
		return this.couleur;
	}

	public boolean getEtat() {
		return this.etat;
	}
	
	public void setDrag (boolean drag) {
		this.drag=drag;
	}
	
	public boolean getDrag() {
		return this.drag;
	}

	public void setPosition (int position) {
		this.position=position;
	}
	
	public int getPosition() {
		return this.position;
	}
}
